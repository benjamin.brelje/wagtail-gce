# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import wagtail.core.fields
import wagtail.core.blocks
import django.db.models.deletion
import wagtail_box.fields


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_auto_20150323_2116'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='blogcategory',
            options={'ordering': ['name'], 'verbose_name_plural': 'Blog Categories', 'verbose_name': 'Blog Category'},
        ),
        migrations.AlterModelOptions(
            name='blogindexpage',
            options={'verbose_name': 'Blog index'},
        ),
        migrations.AlterModelOptions(
            name='blogpage',
            options={'verbose_name_plural': 'Blog pages', 'verbose_name': 'Blog page'},
        ),
        migrations.AddField(
            model_name='blogcategory',
            name='description',
            field=models.CharField(max_length=500, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='blogcategory',
            name='parent',
            field=models.ForeignKey(blank=True, on_delete=models.CASCADE, null=True, to='blog.BlogCategory', help_text='Categories, unlike tags, can have a hierarchy. You might have a Jazz category, and under that have children categories for Bebop and Big Band. Totally optional.'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='blogcategoryblogpage',
            name='category',
            field=models.ForeignKey(verbose_name='Category', on_delete=models.CASCADE, related_name='+', to='blog.BlogCategory'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='blogpage',
            name='body',
            field=wagtail.core.fields.StreamField((('h2', wagtail.core.blocks.CharBlock(icon='title')), ('h3', wagtail.core.blocks.CharBlock(icon='title')), ('paragraph', wagtail.core.blocks.RichTextBlock(icon='pilcrow')), ('image', wagtail.core.blocks.StructBlock((('image', wagtail.images.blocks.ImageChooserBlock()), ('alignment', wagtail_box.fields.ImageFormatBlock()), ('caption', wagtail.core.blocks.RichTextBlock(required=False))), icon='image', label='Aligned image')), ('video', wagtail.core.blocks.StructBlock((('video_link', wagtail.embeds.blocks.EmbedBlock()),), icon='media', label='Embedded video')), ('pullquote', wagtail.core.blocks.StructBlock((('quote', wagtail.core.blocks.TextBlock(label='Quote title')), ('attribution', wagtail.core.blocks.CharBlock())), icon='openquote', label='Quote')))),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='blogpage',
            name='header_image',
            field=models.TextField(max_length=600, verbose_name='Header image', null=True, blank=True),
            preserve_default=True,
        ),
    ]
