# Generated by Django 2.0.9 on 2018-10-16 16:44

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.embeds.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0005_auto_20170905_2044'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homepage',
            name='body',
            field=wagtail.core.fields.StreamField([('h2', wagtail.core.blocks.CharBlock(icon='title')), ('h3', wagtail.core.blocks.CharBlock(icon='title')), ('paragraph', wagtail.core.blocks.RichTextBlock(icon='pilcrow')), ('video', wagtail.core.blocks.StructBlock([('video_link', wagtail.embeds.blocks.EmbedBlock())], icon='media', label='Embedded video')), ('pullquote', wagtail.core.blocks.StructBlock([('quote', wagtail.core.blocks.TextBlock(label='Quote title')), ('attribution', wagtail.core.blocks.CharBlock())], icon='openquote', label='Quote')), ('rawhtml', wagtail.core.blocks.RawHTMLBlock(icon='placeholder', label='Embed HTML')), ('code', wagtail.core.blocks.StructBlock([('language', wagtail.core.blocks.ChoiceBlock(choices=[('python', 'Python'), ('bash', 'Bash/Shell'), ('html', 'HTML'), ('css', 'CSS'), ('scss', 'SCSS')])), ('code', wagtail.core.blocks.TextBlock())], icon='code', label='Embed code'))], null=True),
        ),
    ]
