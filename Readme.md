# Welcome
This is the source for my website. It's built with the DJango framework, the Wagtail CMS, and Bootstrap themes.
It includes configuration details for hosting the website on A Google Compute Engine [Bitnami DjangoStack image](https://docs.bitnami.com/google/infrastructure/django/)

I used the [wagtail-blog project](https://github.com/thelabnyc/wagtail_blog) but changed the RichTextField of the blog
post object with a StreamField and plan on adding some other customizations. I used the StreamField layout from [wagtail-nesting-box](https://github.com/palazzem/wagtail-nesting-box)

## Build details:

- Create a google cloud platform project
- Go to the App Launcher and search 'django'. You should see a Bitnami Djangostack VM appear as an option
- Create the VM
- Once the VM is instantiated, you need the Bitnami root password and the ip address. Both can be found in the GCP console.
- Follow [these instructions](https://cloud.google.com/compute/docs/instances/adding-removing-ssh-keys) to add private keys to the whole project including your VM
- Edit the generic shell build script in this project to include your Bitnami password (from the console) and your Django secrets (MySQL database username and password, SECRET_KEY)
- SSH and run the generic build script AS THE bitnami USER! On windows: plink bitnami@[your-vm-public-ip] -i "C:/Path/to/your/.ppk" -m "C:/Path/to/your/buildscript.sh"
- Details of the build script are commented in. The script pulls down this git repo and configures the vm to serve it properly.
- The project should be running at [your-vm-public-ip]

## How to migrate MySQL database to new instance
Backup:
mysqldump -u root -p django_backend > backup.sql
Restore:
mysql -u root -p django_backend < backup.sql

## Configure encryption
Reference:
ref: https://docs.bitnami.com/bch/how-to/generate-install-lets-encrypt-ssl/

Make sure to: "sudo su bitnami" if you haven't already

- cd /tmp
- curl -s https://api.github.com/repos/xenolf/lego/releases/latest | grep browser_download_url | grep linux_amd64 | cut -d '"' -f 4 | wget -i -
- tar xf lego_vX.Y.Z_linux_amd64.tar.gz
- sudo mv lego /usr/local/bin/lego
- sudo /opt/bitnami/ctlscript.sh stop
- *replace the word DOMAIN with your domain including dot com or whatever*
- sudo lego --email="EMAIL-ADDRESS" --domains="DOMAIN" --domains="www.DOMAIN" --path="/etc/lego" run
- sudo mv /opt/bitnami/apache2/conf/server.crt /opt/bitnami/apache2/conf/server.crt.old
- sudo mv /opt/bitnami/apache2/conf/server.key /opt/bitnami/apache2/conf/server.key.old
- sudo mv /opt/bitnami/apache2/conf/server.csr /opt/bitnami/apache2/conf/server.csr.old
- sudo ln -s /etc/lego/certificates/DOMAIN.key /opt/bitnami/apache2/conf/server.key
- sudo ln -s /etc/lego/certificates/DOMAIN.crt /opt/bitnami/apache2/conf/server.crt
- sudo chown root:root /opt/bitnami/apache2/conf/server*
- sudo chmod 600 /opt/bitnami/apache2/conf/server*
- sudo /opt/bitnami/ctlscript.sh start

## To renew your certs:
- sudo /opt/bitnami/ctlscript.sh stop
- sudo lego --email="EMAIL-ADDRESS" --domains="DOMAIN" --path="/etc/lego" renew
- sudo /opt/bitnami/ctlscript.sh start