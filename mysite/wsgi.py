"""
WSGI config for mysite project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

from __future__ import absolute_import, unicode_literals
import os
import sys
import django


sys.path.append('/opt/bitnami/apps/django/django_projects/mysite')
os.environ.setdefault("PYTHON_EGG_CACHE", "/opt/bitnami/apps/django/django_projects/mysite/egg_cache")

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mysite.settings")

def loading_app(wsgi_environ, start_response):
    global real_app
    for key in ['DJANGO_DB_USERNAME','DJANGO_DB_PASSWORD','DJANGO_SECRET_KEY','DJANGO_DEBUG']:
        try:
            os.environ[key] = wsgi_environ[key]
        except KeyError:
            # The WSGI environment doesn't have the key
            pass
    from django.core.wsgi import get_wsgi_application
    real_app = get_wsgi_application()
    return real_app(wsgi_environ, start_response)

real_app = loading_app

application = lambda env, start: real_app(env, start)