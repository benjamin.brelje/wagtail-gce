#setup script to configure Djangostack for a Git repo-ed wagtail blog

#clone the project
cd /opt/bitnami/apps/django/django_projects/
git clone https://benjamin.brelje@gitlab.com/benjamin.brelje/wagtail-gce.git mysite

#configure the folder permissions and install python packages
cd /opt/bitnami/apps/django/django_projects/mysite
mkdir static
#root has to own the conf folder and contents for some reason
sudo chown -R root conf
sudo pip install -r requirements.txt
sudo pip install six --upgrade
cd /opt/bitnami/apps/django/django_projects/
sudo chmod -R 0755 mysite
#create the backend db and default user
mysql -uroot -p$bitnamipassword -Bse "CREATE DATABASE django_backend CHARACTER SET utf8;CREATE USER '$mydbusername'@'localhost' IDENTIFIED BY '$mydbpassword';GRANT ALL ON django_backend.* TO '$mydbusername'@'localhost';"

#perform django administrative tasks
cd /opt/bitnami/apps/django/django_projects/mysite
#set local environment variables with secrets (duplicate in Apache prod environment below)
export DJANGO_DB_USERNAME=$mydbusername
export DJANGO_DB_PASSWORD=$mydbpassword
export DJANGO_DEBUG=True
export DJANGO_SECRET_KEY=$mysecretkey
#the yes overrides an 'are you sure message'
python3 manage.py collectstatic <<< yes
python3 manage.py migrate
#make a superuser
echo "from django.contrib.auth.models import User; User.objects.create_superuser('$username', '$useremail@domain.com', '$password')" | python3 manage.py shell

#Configure Apache
cd /opt/bitnami/apache2/conf/bitnami/
sudo echo 'Include "/opt/bitnami/apps/django/django_projects/mysite/conf/httpd-prefix.conf"' >> bitnami-apps-prefix.conf
cd /opt/bitnami/apache2/conf/
sudo echo 'SetEnv DJANGO_DB_USERNAME $mydbusername' >> httpd.conf
sudo echo 'SetEnv DJANGO_DB_PASSWORD $mydbpassword' >> httpd.conf
sudo echo 'SetEnv DJANGO_DEBUG False' >> httpd.conf
sudo echo 'SetEnv DJANGO_SECRET_KEY $mysecretkey' >> httpd.conf

/opt/bitnami/ctlscript.sh restart apache

#Configure encryption (only on domain)
# ref: https://docs.bitnami.com/bch/how-to/generate-install-lets-encrypt-ssl/
# make sure to sudo su bitnami if you haven't already

# cd /tmp
# curl -s https://api.github.com/repos/xenolf/lego/releases/latest | grep browser_download_url | grep linux_amd64 | cut -d '"' -f 4 | wget -i -
# # replace the X.Y.Z with latest version of lego
# tar xf lego_vX.Y.Z_linux_amd64.tar.gz
# sudo mv lego /usr/local/bin/lego
# sudo /opt/bitnami/ctlscript.sh stop
# # replace the word DOMAIN with your domain including dot com or whatever
# sudo lego --email="EMAIL-ADDRESS" --domains="DOMAIN" --domains="www.DOMAIN" --path="/etc/lego" run

# sudo mv /opt/bitnami/apache2/conf/server.crt /opt/bitnami/apache2/conf/server.crt.old
# sudo mv /opt/bitnami/apache2/conf/server.key /opt/bitnami/apache2/conf/server.key.old
# sudo mv /opt/bitnami/apache2/conf/server.csr /opt/bitnami/apache2/conf/server.csr.old
# sudo ln -s /etc/lego/certificates/DOMAIN.key /opt/bitnami/apache2/conf/server.key
# sudo ln -s /etc/lego/certificates/DOMAIN.crt /opt/bitnami/apache2/conf/server.crt
# sudo chown root:root /opt/bitnami/apache2/conf/server*
# sudo chmod 600 /opt/bitnami/apache2/conf/server*

# sudo /opt/bitnami/ctlscript.sh start

# # To renew your certs:
# sudo /opt/bitnami/ctlscript.sh stop
# sudo lego --email="EMAIL-ADDRESS" --domains="DOMAIN" --path="/etc/lego" renew
# sudo /opt/bitnami/ctlscript.sh start